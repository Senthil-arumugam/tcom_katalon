import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser and log in
if (!(new tcom.utils().openSite())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch the TCOM application')
}

if (!(new tcom.user().createFirst(null))) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Register the TCOM user')
}

// logout the application
if (!(new tcom.utils().closeSite())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not close the TCOM application')
}