import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser and log in
if (!(new tcom.utils().newSession())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new tcom.search().updatedQtyRemovedCart())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('cannot verify the quantity is changed and the price reflects the change in quantity and item has been removed from the cart')
}

// logout the application
if (!(new tcom.user().logout())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the tcom')
}

