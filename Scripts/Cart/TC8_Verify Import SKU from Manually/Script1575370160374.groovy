import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import internal.GlobalVariable as GlobalVariable

// launch browser and log in
if (!(new tcom.utils().newSession())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new tcom.search().importSKUFromManually(GlobalVariable.strCartSKUs, '8'))) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not  Verify the import SKUs in the from Manually populate the cart')
}

// logout the application
if (!(new tcom.user().logout())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the tcom')
}