import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

import internal.GlobalVariable

// launch browser and log in
if (!(new tcom.utils().newSession())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new tcom.List().VerifyProductAppearsCart(GlobalVariable.strListSKUs , '16'))) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Verify the items from the list added to the cart appear')
}

// logout the application
if (!(new tcom.user().logout())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the tcom')
}