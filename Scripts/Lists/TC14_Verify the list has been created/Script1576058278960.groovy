import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser and log in
if (!(new tcom.utils().newSession())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new tcom.List().VerifyList(null))) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Verify the created list presents')
}

// logout the application
if (!(new tcom.user().logout())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the tcom')
}