import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

import internal.GlobalVariable

// launch browser and log in
if (!(new tcom.utils().newSession())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new tcom.List().removedPartsFromList(null, GlobalVariable.strListSKUs))) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot Verify item is removed from the list')
}

// logout the application
if (!(new tcom.user().logout())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the tcom')
}