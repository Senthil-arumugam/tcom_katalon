import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

import internal.GlobalVariable

// launch browser and log in
if (!(new tcom.utils().newSession())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new tcom.List().duplicateProducts('Duplicate', GlobalVariable.strListSKUs, '8'))) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Create Duplicate list and Verify the Duplicate products populated list')
}

// logout the application
if (!(new tcom.user().logout())) {
    new tcom.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the tcom')
}