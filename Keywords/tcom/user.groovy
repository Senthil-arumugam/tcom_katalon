package tcom
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

class user {


	/**
	 * GET LOGIN NAME
	 * @param teamType - is the name of the team to target... send null if using system created Date or specify prefix in global variables
	 * @return username
	 */
	@Keyword
	def String getLoginName() {

		String strUserName = (new tcom.utils()).getName()
		strUserName = strUserName
		KeywordUtil.logInfo("UserName = " + strUserName)
		return strUserName.toLowerCase()
	}

	/**
	 * getUserName
	 * @return user name of the global variable 
	 */
	@Keyword
	def String getUserName() {

		Date date = new Date()
		String name = ""

		if (GlobalVariable.GlobalLoginName != null) {

			name = name + GlobalVariable.GlobalLoginName
		}
		if (name == ""){
			name = name + date.format('yyyy-MM-dd')
		}

		KeywordUtil.logInfo("UserName = " + name)
		return name.toLowerCase()
	}



	/**
	 * LOGIN USER
	 * @return true if login completed, otherwise false
	 */
	@Keyword
	def login() {
		String userName = ""

		userName = (new tcom.user()).getUserName().trim()
		userName = userName.toLowerCase()

		KeywordUtil.logInfo("User Name : " + userName)
		//check if user name textbox is present on click on logout
		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Sign In"),30)) {
			KeywordUtil.logInfo("Cannot see Sign In")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/Sign In"))) {
			KeywordUtil.logInfo("Cannot click Sign In")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Log In"),30)) {
			KeywordUtil.logInfo("Cannot see Choose Account Continue")
			return false
		}

		//Enter the user name or password do login
		KeywordUtil.logInfo("Logging in user name : "  + userName)
		if (!(new tcom.utils()).sendKeys(findTestObject("Login/User ID"),userName)) {
			KeywordUtil.logInfo("Cannot enter User ID")
			return false
		}

		KeywordUtil.logInfo("Password : " + GlobalVariable.GlobalPassword)
		if (!(new tcom.utils()).sendKeys(findTestObject("Login/Password"),GlobalVariable.GlobalPassword)) {
			KeywordUtil.logInfo("Cannot enter Password")
			return false
		}
		if (!(new tcom.utils()).click(findTestObject("Login/Log In"))) {
			KeywordUtil.logInfo("Cannot click Log In")
			return false
		}

		boolean blnValues = false
		String Account = (new tcom.utils()).GetMultipleObjectDisplay(findTestObject("Login/Account Button"), findTestObject("Login/Choose Account Continue"), 10)

		if(!(Account.equalsIgnoreCase("Account"))){
			blnValues = true
			if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Choose Account Continue"),60)) {
				KeywordUtil.logInfo("Cannot see Choose Account Continue")
				return false
			}

			if (!(new tcom.utils()).click(findTestObject("Login/Choose Account Select"))) {
				KeywordUtil.logInfo("Cannot click Choose Account Select")
				return false
			}

			if (!(new tcom.utils()).click(findTestObject("Login/ChooseAccountSelectSecondValues"))) {
				KeywordUtil.logInfo("Cannot click Choose Account Select Second Values")
				return false
			}

			if (!(new tcom.utils()).click(findTestObject("Login/Choose Account Continue"))) {
				KeywordUtil.logInfo("Cannot click Choose Account Continue")
				return false
			}
		}



		if(blnValues){

			String Location = (new tcom.utils()).GetMultipleObjectDisplay(findTestObject("Login/Account Button"), findTestObject("Login/Choose Location Continue"), 10)

			if(!(Location.equalsIgnoreCase("Account"))){

				if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Choose Location Continue"),30)) {
					KeywordUtil.logInfo("Cannot see Choose Location Continue")
					return false
				}

				/*	if (!(new tcom.utils()).click(findTestObject("Login/Choose Account Select"))) {
				 KeywordUtil.logInfo("Cannot click Choose Account Select")
				 return false
				 }
				 if (!(new tcom.utils()).click(findTestObject("Login/ChooseAccountSelectSecondValues"))) {
				 KeywordUtil.logInfo("Cannot click Choose Account Select Second Values")
				 return false
				 }*/

				if (!(new tcom.utils()).click(findTestObject("Login/Choose Location Continue"))) {
					KeywordUtil.logInfo("Cannot click Choose Location Continue")
					return false
				}
			}

		}


		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Account Button"),60)) {
			KeywordUtil.logInfo("Cannot see Account Button")
			return false
		}

		KeywordUtil.logInfo("User successfully log in the TCOM application")
		return true

	}

	/**
	 * CREATE USER 
	 * @param UserType - Type of user to create (TesscoAdmin, ClientAdmin, Approver, Initiator)
	 * @return Generated Username
	 */
	@Keyword
	def create(String strUserName = null) {

		String struserName =  (new tcom.user()).getLoginName().trim()

		KeywordUtil.logInfo("User Name : " + struserName)

		//check if user name textbox is present on click on logout
		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Sign In"),30)) {
			KeywordUtil.logInfo("Cannot see Sign In")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/Sign In"))) {
			KeywordUtil.logInfo("Cannot click Sign In")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Register"),60)) {
			KeywordUtil.logInfo("Cannot see Choose Account Continue")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/Register"))) {
			KeywordUtil.logInfo("Cannot click Register")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Register/Contact Information Text"),60)) {
			KeywordUtil.logInfo("Cannot see Contact Information Text")
			return false
		}

		KeywordUtil.logInfo("Triggering create credential flow for registration page Under TCOM")

		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/Salutation"),"Mr.")) {
			KeywordUtil.logInfo("Cannot select the Salutation")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/First Name"),"Senthil")) {
			KeywordUtil.logInfo("Cannot enter First Name")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Last Name"),"Arumugam")) {
			KeywordUtil.logInfo("Cannot enter Last Name")
			return false
		}

		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/Your Role"),"System or Network Design")) {
			KeywordUtil.logInfo("Cannot select the Your Role")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Title"),"QA Analyst")) {
			KeywordUtil.logInfo("Cannot enter Title")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Email Address"),GlobalVariable.Email)) {
			KeywordUtil.logInfo("Cannot enter Email Address")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Register/Create Login Text"),2)) {
			KeywordUtil.logInfo("Cannot see Create Login Text")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/User Name"),struserName)) {
			KeywordUtil.logInfo("Cannot enter User Name")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Create a Password"),GlobalVariable.GlobalPassword)) {
			KeywordUtil.logInfo("Cannot enter Create a Password")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Confirm Password"), GlobalVariable.GlobalPassword)) {
			KeywordUtil.logInfo("Cannot enter Confirm Password")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Register/Company Information Text"),2)) {
			KeywordUtil.logInfo("Cannot see Company Information Text")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Company Name"),"Happiest Minds")) {
			KeywordUtil.logInfo("Cannot enter Company Name")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Phone Number"),GlobalVariable.SmsPhoneNumber)) {
			KeywordUtil.logInfo("Cannot enter Phone Number")
			return false
		}


		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/Type of Business"),"Enterprise")) {
			KeywordUtil.logInfo("Cannot select the Type of Business")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Address One"),"375 West Padonia Rd")) {
			KeywordUtil.logInfo("Cannot enter Address One")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Address Two"),"")) {
			KeywordUtil.logInfo("Cannot enter Address Two")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/City"),"Timonium")) {
			KeywordUtil.logInfo("Cannot enter City")
			return false
		}

		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/State"),"Maryland")) {
			KeywordUtil.logInfo("Cannot select the State")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/ZIP OR Postal Code"),"21093")) {
			KeywordUtil.logInfo("Cannot enter ZIP OR Postal Code")
			return false
		}

		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/Country"),"United States")) {
			KeywordUtil.logInfo("Cannot select the Country")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Register/Receive Emails"))) {
			KeywordUtil.logInfo("Cannot click Receive Emails")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Register/Accepted Terms Conditions"))) {
			KeywordUtil.logInfo("Cannot click Accepted Terms Conditions")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Register/Submit Registration"))) {
			KeywordUtil.logInfo("Cannot click Submit Registration")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Register/Congratulations"),60)) {
			KeywordUtil.logInfo("Cannot see Congratulations")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Register/Continue"))) {
			KeywordUtil.logInfo("Cannot click Continue")
			return false
		}


		KeywordUtil.logInfo("Successfully credentials are created under TCOM")
		return true
	}


	/**
	 * CREATE USER
	 * @param UserType - Type of user to create (TesscoAdmin, ClientAdmin, Approver, Initiator)
	 * @return Generated Username
	 */
	@Keyword
	def createFirst(String strUserName = null) {

		String struserName =  (new tcom.user()).getLoginName().trim()

		KeywordUtil.logInfo("User Name : " + struserName)

		//check if user name textbox is present on click on logout
		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Sign In"),30)) {
			KeywordUtil.logInfo("Cannot see Sign In")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/Sign In"))) {
			KeywordUtil.logInfo("Cannot click Sign In")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Register"),60)) {
			KeywordUtil.logInfo("Cannot see Choose Account Continue")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/Register"))) {
			KeywordUtil.logInfo("Cannot click Register")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Register/Contact Information Text"),60)) {
			KeywordUtil.logInfo("Cannot see Contact Information Text")
			return false
		}

		KeywordUtil.logInfo("Triggering create credential flow for registration page Under TCOM")

		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/Salutation"),"Mr.")) {
			KeywordUtil.logInfo("Cannot select the Salutation")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/First Name"),"Senthil")) {
			KeywordUtil.logInfo("Cannot enter First Name")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Last Name"),"Arumugam")) {
			KeywordUtil.logInfo("Cannot enter Last Name")
			return false
		}

		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/Your Role"),"System or Network Design")) {
			KeywordUtil.logInfo("Cannot select the Your Role")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Title"),"QA Analyst")) {
			KeywordUtil.logInfo("Cannot enter Title")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Email Address"),GlobalVariable.Email)) {
			KeywordUtil.logInfo("Cannot enter Email Address")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Register/Create Login Text"),2)) {
			KeywordUtil.logInfo("Cannot see Create Login Text")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/User Name"),struserName)) {
			KeywordUtil.logInfo("Cannot enter User Name")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Create a Password"),GlobalVariable.GlobalPassword)) {
			KeywordUtil.logInfo("Cannot enter Create a Password")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Confirm Password"), GlobalVariable.GlobalPassword)) {
			KeywordUtil.logInfo("Cannot enter Confirm Password")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Register/Company Information Text"),2)) {
			KeywordUtil.logInfo("Cannot see Company Information Text")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Company Name"),"Happiest Minds")) {
			KeywordUtil.logInfo("Cannot enter Company Name")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Phone Number"),GlobalVariable.SmsPhoneNumber)) {
			KeywordUtil.logInfo("Cannot enter Phone Number")
			return false
		}


		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/Type of Business"),"Enterprise")) {
			KeywordUtil.logInfo("Cannot select the Type of Business")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Address One"),"375 West Padonia Rd")) {
			KeywordUtil.logInfo("Cannot enter Address One")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/Address Two"),"")) {
			KeywordUtil.logInfo("Cannot enter Address Two")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/City"),"Timonium")) {
			KeywordUtil.logInfo("Cannot enter City")
			return false
		}

		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/State"),"Maryland")) {
			KeywordUtil.logInfo("Cannot select the State")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Register/ZIP OR Postal Code"),"21093")) {
			KeywordUtil.logInfo("Cannot enter ZIP OR Postal Code")
			return false
		}

		if (!(new tcom.utils()).selectDropDownValue(findTestObject("Register/Country"),"United States")) {
			KeywordUtil.logInfo("Cannot select the Country")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Register/Receive Emails"))) {
			KeywordUtil.logInfo("Cannot click Receive Emails")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Register/Accepted Terms Conditions"))) {
			KeywordUtil.logInfo("Cannot click Accepted Terms Conditions")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Register/Submit Registration"))) {
			KeywordUtil.logInfo("Cannot click Submit Registration")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		KeywordUtil.logInfo("Successfully credentials are created under TCOM")
		return true
	}



	/**
	 * changePassword 
	 * @param UserType - Type of user to create (TesscoAdmin, ClientAdmin, Approver, Initiator)
	 * @return Generated Username
	 */
	@Keyword
	def changePassword() {

		String struserName =  (new tcom.user()).getLoginName().trim()

		//check if user name textbox is present on click on logout
		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Sign In"),30)) {
			KeywordUtil.logInfo("Cannot see Sign In")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/Sign In"))) {
			KeywordUtil.logInfo("Cannot click Sign In")
			return false
		}

		KeywordUtil.logInfo("Triggering On Reset Password")


		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/Forgot Reset password"),60)) {
			KeywordUtil.logInfo("Cannot see Forgot Reset password")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/Forgot Reset password"))) {
			KeywordUtil.logInfo("Cannot click Forgot Reset password")
			return false
		}


		if (!(new tcom.utils()).refresh()) {
			KeywordUtil.logInfo("Cannot refresh page")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Change Password/Submit"),60)) {
			KeywordUtil.logInfo("Cannot see Submit")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Change Password/Forgot Password"),2)) {
			KeywordUtil.logInfo("Cannot see Forgot Password")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Change Password/User Name TextBox"),struserName)) {
			KeywordUtil.logInfo("Cannot enter User Name")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Change Password/Submit"))) {
			KeywordUtil.logInfo("Cannot click Submit")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Change Password/Success"),60)) {
			KeywordUtil.logInfo("Cannot see Success")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Change Password/Please Check Your Email"),2)) {
			KeywordUtil.logInfo("Cannot see Please Check Your Email")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Change Password/Log In"))) {
			KeywordUtil.logInfo("Cannot click Log In")
			return false
		}

		KeywordUtil.logInfo("Successfully reset the password and link is send Via email")
		return true
	}




	/**
	 * logout USER
	 * @return true if login completed, otherwise false
	 */

	@Keyword
	def logout() {

		KeywordUtil.logInfo("User logout operation is triggered")

		if (!(new tcom.utils()).delay(7)) {
			KeywordUtil.logInfo("Cannot Wait a sec")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/AccountAndLocationText"),30)) {
			KeywordUtil.logInfo("Cannot see Account Button")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/AccountAndLocationText"))) {
			KeywordUtil.logInfo("Cannot click Choose Location Continue")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/LogOut"))) {
			KeywordUtil.logInfo("Cannot click Choose Location Continue")
			return false
		}

		if (!(new tcom.utils()).delay(3)) {
			KeywordUtil.logInfo("Cannot Wait a sec")
			return false
		}

		if (!(new tcom.utils()).closeSite()) {
			KeywordUtil.logInfo("Cannot close the application")
			return false
		}

		KeywordUtil.logInfo("User log out the tcom application")
		return true
	}

}
