package tcom
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.parser.PdfTextExtractor
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

class utils {


	/**
	 * @author Senthil.Arumugam
	 * writeToCSVFile(String filePath, String ProjectName, String DeliveryDate, String Summary,String Value)
	 * https://www.logicbig.com/tutorials/misc/groovy/data-type-and-variables.html
	 * @param to CSV filePath, ProjectName, DeliveryDate, Summary and Value
	 * @return True if CSV file is write successfully otherwise will return false  
	 */
	@Keyword
	def writeToCSVFile(def csvPath = null, def partsName = null, def qualitySize = null) {

		try {
			def list = Arrays.asList(Arrays.asList(partsName, qualitySize))

			FileWriter csvWriter = new FileWriter(csvPath)

			csvWriter.append('Sku')

			csvWriter.append(',')

			csvWriter.append('Quantity')

			csvWriter.append('\n')

			for (def listData : list) {
				csvWriter.append(String.join(',', listData))

				csvWriter.append('\n')
			}

			csvWriter.flush()

			csvWriter.close()
			return true
		} catch (Exception e) {
			println e
			KeywordUtil.logInfo("Failed to update CSV file path : " + csvPath)
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * ReadCSVFile(String strFilePath, int iRow)
	 * @param strFilePath and iRow
	 * @return True if CSV file is read specific line successfully otherwise will return false
	 */
	@Keyword
	def ReadCSVFile(String strFilePath, int iRow) {
		try {
			ArrayList<List> records = new ArrayList()
			BufferedReader br = new BufferedReader(new FileReader(strFilePath))
			String line

			while ((line = br.readLine()) != null) {

				String[] values = line.split(',')
				records.add(Arrays.asList(values))
			}

			return records.get(iRow)
		} catch (Exception e) {

			KeywordUtil.logInfo("Can not read the csv file : " + strFilePath)
			return 0
		}
		return 0
	}


	/**
	 * @author Senthil.Arumugam
	 * ReadPDFFile(String strFilePath, String strExpectedValue)
	 * @param strFilePath and strExpectedValue
	 * @return True if PDF file is read and compared to specific values otherwise will return false
	 */
	@Keyword
	def ReadPDFFile(String strFilePath, String strExpectedValue) {

		Boolean blnValues = false

		try {
			//Create PdfReader instance.
			PdfReader pdfReader = new PdfReader(strFilePath)

			//Get the number of pages in pdf.
			int pages = pdfReader.getNumberOfPages()

			//Iterate the pdf through pages.
			for (int i = 1; i <= pages; i++) {
				//Extract the page content using PdfTextExtractor.
				String pageContent = PdfTextExtractor.getTextFromPage(pdfReader, i)

				if (pageContent.contains(strExpectedValue)) {
					blnValues = true
				}
				//Print the page content on console.
				KeywordUtil.logInfo("Content on Page " + i + ": " + pageContent)
			}

			//Close the PdfReader.
			pdfReader.close()

		} catch (Exception e) {

			KeywordUtil.logInfo("Can not read the PDF file : " + strFilePath)
			return blnValues
		}
		return blnValues
	}

	/**
	 * @author Senthil.Arumugam
	 * ReadExcelFile(String strFilePath, String strExpText)
	 * @param strFilePath and strExpText
	 * @return True if Excel file is read and compared to specific values otherwise will return false
	 */
	@Keyword
	def ReadExcelFile(String strFilePath, String strExpText) {

		Boolean blnValues = false

		try {

			FileInputStream fis = new FileInputStream(strFilePath)

			XSSFWorkbook workbook = new XSSFWorkbook(fis)

			XSSFSheet sheet = workbook.getSheetAt(0)

			int rowsCount = sheet.getLastRowNum()

			KeywordUtil.logInfo("Total Number of Rows : " +  rowsCount + 1 )

			for (int i = 0; i <= rowsCount; i++) {
				Row row = sheet.getRow(i)

				int colCounts = row.getLastCellNum()

				KeywordUtil.logInfo("Total Number of Cols : " + colCounts)

				for (int j = 0; j < colCounts; j++) {
					Cell cell = row.getCell(j)

					if(cell.getStringCellValue().contains(strExpText)){
						blnValues = true
					}
				}
			}

			return blnValues
		} catch (Exception e) {
			KeywordUtil.logInfo("Can not read the csv file : " + strFilePath)

			return blnValues
		}

		return blnValues
	}


	/**
	 * @author Senthil.Arumugam
	 * WriteExcelFile(String strFilePath, String strSheetName, def strSheetOneData, def strSheetTwoData) 
	 * @param strFilePath and strSheetName and strSheetOneData and strSheetTwoData
	 * @return True if Excel file is write two sheets otherwise will return false
	 */
	@Keyword
	def WriteExcelFile(String strFilePath, String strSheetName, def strSheetOneData, def strSheetTwoData) {

		Boolean blnValues = false

		def i = 0

		XSSFWorkbook workbook = new XSSFWorkbook()

		String[] str = strSheetName.split('##')

		for (String values : str) {
			i = (i + 1)

			def datatypes

			XSSFSheet sheet = workbook.createSheet(values)

			if (i == 1) {
				datatypes = strSheetOneData
			} else {
				datatypes = strSheetTwoData
			}

			int rowNum = 0

			KeywordUtil.logInfo("Creating excel")

			for (Object[] datatype : datatypes) {
				Row row = sheet.createRow(rowNum++)

				int colNum = 0

				for (Object field : datatype) {
					Cell cell = row.createCell(colNum++)

					if (field instanceof String) {
						cell.setCellValue(((field) as String))
					} else if (field instanceof Integer) {
						cell.setCellValue(((field) as Integer))
					}
					blnValues = true
				}
			}
		}

		try {
			FileOutputStream outputStream = new FileOutputStream(strFilePath)

			workbook.write(outputStream)

			workbook.close()
		}
		catch (FileNotFoundException e) {
			KeywordUtil.logInfo(e.printStackTrace())
		}
		catch (IOException e) {
			KeywordUtil.logInfo(e.printStackTrace())
		}

		KeywordUtil.logInfo("Excel sheet is created")

		return blnValues
	}


	/**
	 * @author Senthil.Arumugam
	 * takeScreenShot()
	 * @return true if script take a screenshot otherwise false
	 */

	@Keyword
	def takeScreenShot() {
		String	path=""
		try {
			def pool = [('a'..'z'), ('A'..'Z'), (0..9), '-'].flatten()

			Random random = new Random(System.currentTimeMillis())

			def randomChars = (0..30 - 1).collect({
				pool[random.nextInt(pool.size())]
			})
			println(randomChars)
			def randomString = randomChars.join()
			path = System.getProperty("user.dir")+'\\Reports\\ScreenShot\\' + randomString+'.png'
			WebUI.takeScreenshot(path)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("cannot take screenshot path is : " + path)
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * sendKeys(TestObject to, String keys)
	 * @param TestObject and keys
	 * @return True if values are entering into specific object otherwise will return false
	 */	
	@Keyword
	def sendKeys(TestObject to, String keys) {
		try {
			if(verifyVisibleOfElement(to,1)){
				WebElement element = WebUI.findWebElement(to)
				element.sendKeys(keys)
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to sendkeys without clear to object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * getURL() 
	 * @return True if URL is available on application otherwise will return false
	 */
	@Keyword
	def getURL() {
		try {

			return  WebUI.getUrl()

		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get current URL on page")
			return ""
		}
		return ""
	}


	/**
	 * @author Senthil.Arumugam
	 * sendKeysOutWait(TestObject to, String keys)
	 * @param TestObject and keys
	 * @return True if values are entering into specific object otherwise will return false
	 */
	@Keyword
	def sendKeysOutWait(TestObject to, String keys) {
		try {
			WebUI.sendKeys(to, keys)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to sendkeys waithout wait : " + to.getObjectId())
			return false
		}
		return false
	}



	/**
	 * @author Senthil.Arumugam
	 * waitForPageLoad(int time)
	 * @param time
	 * @return True if page loaded within expected time otherwise will return false
	 */
	@Keyword
	def waitForPageLoad(int time) {
		try {
			WebUI.waitForPageLoad(time)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to load a page")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * clearsendKeys(TestObject to, String keys)
	 * @param TestObject and keys
	 * @return True if values are entering into specific object after clearing old values otherwise will return false
	 */
	@Keyword
	def clearsendKeys(TestObject to, String keys) {
		try {
			if(verifyVisibleOfElement(to,1)){
				WebElement element = WebUI.findWebElement(to)
				element.clear()
				element.sendKeys(keys)
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to sendkeys to object: " + to.getObjectId())
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * delay(int time)
	 * @param time
	 * @return True if script is waiting particular time without any expectation otherwise will return false
	 */
	@Keyword
	def delay(int time) {
		try {
			WebUI.delay(time)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to wait a time  " + time)
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * getCSSValue(TestObject to, String strValues, String strExpectedValues) 
	 * @param TestObject and strValues and strExpectedValues
	 * @return True if user able to get specific object CSS values otherwise will return false
	 */
	@Keyword
	def getCSSValue(TestObject to, String strValues, String strExpectedValues) {
		try {
			if(verifyVisibleOfElement(to,5)){
				if(WebUI.getCSSValue(to, strValues).trim().equalsIgnoreCase(strExpectedValues)){
					return true
				}
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get CSS values object: " + to.getObjectId())
			return false
		}
		return false
	}



	/**
	 * @author Senthil.Arumugam
	 * click(TestObject to)
	 * @param TestObject
	 * @return True if user able to click specific object otherwise will return false
	 */
	@Keyword
	def click(TestObject to) {
		try {
			if(verifyVisibleOfElement(to,10)){
				WebElement element = WebUI.findWebElement(to)
				element.click()

				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to click object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * GetMultipleObjectDisplay(TestObject to1, TestObject to2, int iWaitTime)
	 * @param TestObject
	 * @return True if user able to click specific object otherwise will return false
	 */
	@Keyword
	def GetMultipleObjectDisplay(TestObject to1, TestObject to2, int iWaitTime) {
		String blnValue = "None"
		ArrayList<WebElement> lst = null
		ArrayList<WebElement> lst1 = null



		for (int i=0;i<iWaitTime;i++) {
			try {
				String strXpathLocator = ""
				String strXpath = to1.getSelectorCollection()
				String strLastIndex = strXpath.substring(0, strXpath.length() - 1)
				String strXpathRemove = strLastIndex.replaceAll('XPATH=', '')
				strXpathRemove = strXpathRemove.replaceAll(', CSS=', '')
				strXpathRemove = strXpathRemove.replaceAll(', BASIC=', '')
				strXpathRemove = strXpathRemove.replaceAll('BASIC=,', '')
				strXpathRemove = strXpathRemove.replaceAll('CSS=,', '')

				strXpathLocator = strXpathRemove.substring(1)
				WebDriver driver = DriverFactory.getWebDriver()

				lst1  = driver.findElements(By.xpath(strXpathLocator))
				if(lst1.size() == 1){
					blnValue = "Account"
					break
				}
			} catch (Exception e) {
				KeywordUtil.logInfo("Failed to see object: " + to1.getObjectId())

			}

			try {
				String strXpathLocatorto2 = ""
				String strXpathto2 = to2.getSelectorCollection()
				String strLastIndexto2 = strXpathto2.substring(0, strXpathto2.length() - 1)
				String strXpathRemoveto2 = strLastIndexto2.replaceAll('XPATH=', '')
				strXpathRemoveto2 = strXpathRemoveto2.replaceAll(', CSS=', '')
				strXpathRemoveto2 = strXpathRemoveto2.replaceAll(', BASIC=', '')
				strXpathRemoveto2 = strXpathRemoveto2.replaceAll('BASIC=,', '')
				strXpathRemoveto2 = strXpathRemoveto2.replaceAll('CSS=,', '')
				strXpathLocatorto2 = strXpathRemoveto2.substring(1)
				WebDriver driver = DriverFactory.getWebDriver()

				lst  = driver.findElements(By.xpath(strXpathLocatorto2))

				if(lst.size() == 1){
					if(lst.get(0).displayed){
						blnValue = "Other"
						break
					}
				}
			} catch (Exception e) {
				KeywordUtil.logInfo("Failed to see object: " + to2.getObjectId())

			}

			WebUI.delay(5)
		}

		return blnValue
	}


	/**
	 * @author Senthil.Arumugam
	 * listWebElement(String to)
	 * @param strXpath
	 * @return True if specific locator able to return a list of values otherwise will return false
	 */
	@Keyword
	def listWebElement(String to) {
		ArrayList<WebElement> lst = null
		try {
			WebDriver driver = DriverFactory.getWebDriver()
			lst = driver.findElements(By.xpath(to))
			return lst
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to return list of values object: " + to)
			return lst
		}
		return lst
	}

	/**
	 * @author Senthil.Arumugam
	 * getText(TestObject to,String strName)
	 * @param TestObject and strName
	 * @return True if able to get text for specific test object otherwise will return false
	 */
	@Keyword
	def  getText(TestObject to,String strName) {
		try {
			if(verifyVisibleOfElement(to,10)){
				KeywordUtil.logInfo(WebUI.getText(to))
				KeywordUtil.logInfo(strName)
				if (WebUI.getText(to).trim().equalsIgnoreCase(strName)) {
					return true
				}
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get text object: " + to.getObjectId())
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * sReturnText(TestObject to)
	 * @param TestObject
	 * @return True if able to get text for specific test object otherwise will return false
	 */
	@Keyword
	def sReturnText(TestObject to) {
		try {
			if(verifyVisibleOfElement(to,10)){
				KeywordUtil.logInfo(WebUI.getText(to))
				return WebUI.getText(to).trim()
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get text object: " + to.getObjectId())
			return ""
		}
		return ""
	}

	/**
	 * @author Senthil.Arumugam
	 * GetAttributeText(TestObject to,String strAttribute)
	 * @param TestObject and strAttribute
	 * @return True if able to get attribute for specific test object otherwise will return empty values
	 */
	@Keyword
	String  GetAttributeText(TestObject to,String strAttribute) {
		try {
			if(verifyVisibleOfElement(to,10)){
				return WebUI.getAttribute(to, strAttribute).trim()
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get Attribute object: " + to.getObjectId())
			return ""
		}
		return ""
	}




	/**
	 * @author Senthil.Arumugam
	 * selectDropDownValue(TestObject to, String labelName)
	 * @param TestObject and labelName
	 * @return True if able to select dropdown values for specific test object otherwise will return false
	 */
	@Keyword
	def selectDropDownValue(TestObject to, String labelName) {

		try {
			if(verifyVisibleOfElement(to,5)){
				WebUI.selectOptionByLabel(to, labelName, false)
				WebUI.delay(1)
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to select drop down values object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * selectDropDownIndex(TestObject to, String labelName)
	 * @param TestObject and labelName
	 * @return True if able to select dropdown values for specific test object otherwise will return false
	 */
	@Keyword
	def selectDropDownIndex(TestObject to, String labelName) {

		try {
			if(verifyVisibleOfElement(to,5)){
				WebUI.selectOptionByIndex(to, labelName, false)
				WebUI.delay(1)
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to select drop down index object: " + to.getObjectId())
			return false
		}
		return false
	}



	/**
	 * @author Senthil.Arumugam
	 * scrollToElement(TestObject to)
	 * @param TestObject
	 * @return True if able to scroll for specific test object otherwise will return false
	 */
	@Keyword
	def scrollToElement(TestObject to) {
		try {
			WebUI.scrollToElement(to,10)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to scroll to object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * verifyPresent(TestObject to, int timeout)
	 * @param TestObject and timeout 
	 * @return True if able to display for specific test object otherwise will return false
	 */
	@Keyword
	def verifyPresent(TestObject to, int timeout){
		try {
			WebElement element = WebUI.findWebElement(to, timeout)
			if(element.displayed){
				return true
			}
		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getObjectId() + " not displayed")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * verifyVisibleOfElement(TestObject to, int timeout)
	 * @param TestObject and timeout
	 * @return True if element to be visible for specific test object otherwise will return false
	 */
	@Keyword
	def verifyVisibleOfElement(TestObject to, int timeout){
		try {
			if(WebUI.waitForElementVisible(to, timeout)){
				return true
			}
		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getObjectId() + " Element not Visible")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * verifyElementDidNotDisplayed(String strXPath = null)
	 * @param strXPath - user should pass the xpath 
	 * @return True if element to be Displayed for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementDidNotDisplayed(String strXPath = null){
		try {
			Boolean blnFlag = true
			String strXpathLocator = ""
			int iIncrement = 0

			if(strXPath ==  null){
				strXpathLocator = "//div[@class='square second']"
			} else {
				strXpathLocator = strXPath
			}

			WebUI.delay(3)

			while (blnFlag) {

				WebDriver driver = DriverFactory.getWebDriver()

				iIncrement = iIncrement + 1
				int iSize = driver.findElements(By.xpath(strXpathLocator)).size()

				if(!(iSize == 0)){

					WebElement element = driver.findElement(By.xpath(strXpathLocator))
					if (element.isDisplayed()) {
						WebUI.delay(3)
					} else {
						blnFlag = false
						return true
					}

					if (iIncrement == 30) {
						KeywordUtil.logInfo(strXPath + " locator did not Present and time is out")
						blnFlag = false
					}
				}else {
					blnFlag = false
					return true
				}

			}

		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(strXPath + " Element not Visible")
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * verifyElementIsDisplayed(String strXPath = null)
	 * @param strXPath - user should pass the xpath
	 * @return True if element to be Displayed for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementIsDisplayed(TestObject to, int iWaitTime = null){
		try {

			boolean blnFlag = false
			String strXpathLocator = ""
			String strXpath = to.getSelectorCollection()
			String strLastIndex = strXpath.substring(0, strXpath.length() - 1)
			String strXpathRemove = strLastIndex.replaceAll('XPATH=', '')
			strXpathLocator = strXpathRemove.substring(1)

			for (int i = 1; i <= iWaitTime; i++) {

				WebDriver driver = DriverFactory.getWebDriver()
				int iSize = driver.findElements(By.xpath(strXpathLocator)).size()
				WebUI.delay(1)


				if(!(iSize == 0)){
					WebElement element = driver.findElement(By.xpath(strXpathLocator))
					if (element.isDisplayed()) {
						blnFlag = true
						break
					}
				}
			}

			return blnFlag

		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getSelectorCollection() + " Element not Visible")
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * refresh
	 * @param 
	 * @return True if page is refreshed otherwise will return false
	 */
	@Keyword
	def refresh(){
		try {

			WebUI.refresh()
			return true

		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo("unable to reload the Element")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * verifyElementNotPresent(TestObject to, int timeout)
	 * @param TestObject and timeout 
	 * @return True if element not visible for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementNotPresent(TestObject to, int timeout){
		try {
			WebUI.delay(2)
			if(WebUI.waitForElementNotPresent(to, timeout)){
				return true
			}
		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getObjectId() + " Element Present")
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * verifyElementChecked(TestObject to)
	 * @param TestObject
	 * @return True if Element to checked for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementChecked(TestObject to) {
		try {
			if(verifyVisibleOfElement(to,1)){
				boolean result = WebUI.verifyElementChecked(to, 1)
				if (!result){
					WebUI.findWebElement(to).click()
				}
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to verify Element Checked object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * getName()
	 * @return True if name is created otherwise will return empty values
	 */
	@Keyword
	def String getName() {

		Date date = new Date()
		String name = ""

		if (GlobalVariable.GlobalUserName != null) {

			name = name + GlobalVariable.GlobalUserName
		}
		if (name == ""){
			name = name + date.format('yyyy-MM-dd')
		}
		return name
	}


	/**
	 * @author Senthil.Arumugam
	 * newSession()
	 * @return True if specific site is open and login is success otherwise will return false
	 */
	@Keyword
	def newSession() {
		if (!openSite()) {
			return false
		}
		if (!(new tcom.user()).login()) {
			return false
		}
		return true
	}

	/**
	 * @author Senthil.Arumugam
	 * openSite()
	 * @return True if site is opened and maximized windows otherwise will return false
	 */
	@Keyword
	def openSite() {
		KeywordUtil.logInfo("Opening Site" + GlobalVariable.TestSite)
		try {
			WebUI.openBrowser(GlobalVariable.TestSite)
			WebUI.maximizeWindow()
			return true
		} catch (Exception e) {
			return false
		}
	}

	/**
	 * @author Senthil.Arumugam
	 * openSite()
	 * @return True if site is closed otherwise will return false
	 */
	@Keyword
	def closeSite() {
		KeywordUtil.logInfo("Close Site" + GlobalVariable.TestSite)
		try {
			WebUI.closeBrowser()
			return true
		} catch (Exception e) {
			return false
		}
	}

	/**
	 * @author Senthil.Arumugam
	 * advancePagination(int PageNum)
	 * @param PageNum
	 * @return true if element present, otherwise false
	 */
	@Keyword
	def advancePagination(int PageNum) {

		KeywordUtil.logInfo("Util advancePagination triggered")

		TestObject to = new TestObject("Pagination")
		String xpath = "//a[normalize-space(text()) = '" + PageNum + "']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		try {
			WebElement element = WebUI.findWebElement(to,3)
			element.click()
			WebUI.delay(1)
			KeywordUtil.logInfo("Pagination Advanced")
			return true
		}
		catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo("Pagination can not be advanced")
			return false
		}
		catch (Exception e) {
			KeywordUtil.logInfo("Pagination advancing error")
			return false
		}
	}


}