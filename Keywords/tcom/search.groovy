package tcom
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.ArrayList

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.stringtemplate.v4.compiler.STParser.ifstat_return

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory

class search {


	/**
	 * GET LOGIN NAME
	 * @param teamType - is the name of the team to target... send null if using system created Date or specify prefix in global variables
	 * @return username
	 */
	@Keyword
	def String getLoginName() {

		String strUserName = (new tcom.utils()).getName()
		strUserName = strUserName
		KeywordUtil.logInfo("UserName = " + strUserName)
		return strUserName.toLowerCase()
	}


	/**
	 * searchParts(String Search = null, String PageSize = null)
	 * @param Search - User should send a search for parts name
	 * @param PageSize - User should send a parts display size
	 * @return true if parts is searched and add to cart, otherwise false
	 */

	@Keyword
	def searchParts(String Search = null, int PageSize = null) {

		if(Search == null){
			Search = "Cable"
		}

		KeywordUtil.logInfo("Search product and validation product details is triggered")

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Search button"),20)) {
			KeywordUtil.logInfo("Cannot see the Search Button")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Search/Search Textbox"), Search)) {
			KeywordUtil.logInfo("Cannot enter Search text under Dashboard")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Search/Search Button"))) {
			KeywordUtil.logInfo("Cannot click Search Button")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Search Button"),30)) {
			KeywordUtil.logInfo("Cannot see the Search Button")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		String strXpath="//*[contains(text(),'Search Results for')]/following::a[@class='productName CoveoResultLink hidden-xs']"
		int  lstProductSize = (new tcom.utils()).listWebElement(strXpath).size()

		if (!(lstProductSize <= PageSize)) {
			KeywordUtil.logInfo("Cannot see a search Page Size")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Search/Product Name First"))) {
			KeywordUtil.logInfo("Cannot click Product Name First")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Add to Cart"),30)) {
			KeywordUtil.logInfo("Cannot see the Add to Cart")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/TESSCO SKU"),1)) {
			KeywordUtil.logInfo("Cannot see the TESSCO SKU")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/UPC"),1)) {
			KeywordUtil.logInfo("Cannot see the UPC")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/QTY or UOM"),1)) {
			KeywordUtil.logInfo("Cannot see the QTY or UOM")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/MFG PART"),1)) {
			KeywordUtil.logInfo("Cannot see the Add to Cart")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Description Section"),1)) {
			KeywordUtil.logInfo("Cannot see the Description Section")
			return false
		}


		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Technical Specs"),1)) {
			KeywordUtil.logInfo("Cannot see the Technical Specs")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Manufacturer"),1)) {
			KeywordUtil.logInfo("Cannot see the Manufacturer")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Qty Uom"),1)) {
			KeywordUtil.logInfo("Cannot see the Qty Uom")
			return false
		}

		if(!(Search.contains("Flex"))) {

			if (!(new tcom.utils()).click(findTestObject("Search/Add to Cart"))) {
				KeywordUtil.logInfo("Cannot click Add to Cart")
				return false
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}

			if ((new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Add to Cart"), 10)) {

				if (!(new tcom.search()).VerifyCableCut()) {
					KeywordUtil.logInfo("Cannot Add Reel Lengths in CableCut")
					return false
				}
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}

			if (!(new tcom.utils()).delay(7)) {
				KeywordUtil.logInfo("Cannot Wait a sec")
				return false
			}
		}

		KeywordUtil.logInfo("Searched product and validation is done in product details")
		return true
	}

	/**
	 * verifyOrder()
	 * @return true if order can be placed successfully, otherwise false
	 */

	@Keyword
	def verifyOrder() {
		KeywordUtil.logInfo("Verify an order can be placed successfully")

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Search/Cart"),30)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Search/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Print Quote"),30)) {
			KeywordUtil.logInfo("Cannot see the Print Quote")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Checkout"))) {
			KeywordUtil.logInfo("Cannot click Checkout")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if ((new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Accept All and Continue"),30)) {

			if (!(new tcom.utils()).click(findTestObject("Cart/Accept All and Continue"))) {
				KeywordUtil.logInfo("Cannot click Accept All and Continue")
				return false
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Proceed to Delivery Options"),30)) {
			KeywordUtil.logInfo("Cannot see the Proceed to Delivery Options")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Proceed to Delivery Options"))) {
			KeywordUtil.logInfo("Cannot click the Proceed to Delivery Options")
			return false
		}


		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if ((new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Proceed to Delivery Options Two"),30)) {
			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}

			if (!(new tcom.utils()).click(findTestObject("Cart/Proceed to Delivery Options Two"))) {
				KeywordUtil.logInfo("Cannot click the Proceed to Delivery Options Two")
				return false
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}
		}



		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Proceed to Payment"),30)) {
			KeywordUtil.logInfo("Cannot see the In Proceed to Payment")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Proceed to Payment"))) {
			KeywordUtil.logInfo("Cannot click Proceed to Payment")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Proceed to Review Order"),30)) {
			KeywordUtil.logInfo("Cannot see the Proceed to Review Order")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Credit Card"))) {
			KeywordUtil.logInfo("Cannot click Credit Card")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Cart/Card Name"), "MasterCard")) {
			KeywordUtil.logInfo("Cannot enter Card Name")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Cart/Card Number"), "5500000000000004")) {
			KeywordUtil.logInfo("Cannot enter Card Number")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Cart/Card Month"), "11")) {
			KeywordUtil.logInfo("Cannot enter Card Month")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Cart/Card year"), "2029")) {
			KeywordUtil.logInfo("Cannot enter Card year")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Proceed to Review Order"))) {
			KeywordUtil.logInfo("Cannot click Proceed to Review Order")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Place Order"),30)) {
			KeywordUtil.logInfo("Cannot see the Place Order")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Place Order"))) {
			KeywordUtil.logInfo("Cannot click Place Order")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Order Confirmation"),30)) {
			KeywordUtil.logInfo("Cannot see the Order Confirmation")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Cart/Thank you for your purchase"),1)) {
			KeywordUtil.logInfo("Cannot see the Thank you for your purchase")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Continue Shopping"))) {
			KeywordUtil.logInfo("Cannot click Continue Shopping")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Search Result"),30)) {
			KeywordUtil.logInfo("Cannot see the Search Result")
			return false
		}

		KeywordUtil.logInfo("Verified an order is successfully placed")
		return true
	}

	/**
	 * VerifyCableCut(String Search = null)
	 * @param Search - User should send a search for parts name
	 * @return true if product appears in the cart, otherwise false
	 */

	@Keyword
	def VerifyCableCut() {

		KeywordUtil.logInfo("Verify product appears in the cart")

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).clearsendKeys(findTestObject("Cart/Number of Cuts"), "1")) {
			KeywordUtil.logInfo("Cannot enter Number of Cuts")
			return false
		}

		if (!(new tcom.utils()).clearsendKeys(findTestObject("Cart/Feet"), "100")) {
			KeywordUtil.logInfo("Cannot enter Feet")
			return false
		}


		if (!(new tcom.utils()).click(findTestObject("Cart/Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click Add to Cart")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		KeywordUtil.logInfo("End Search product under My tcom page")
		return true
	}



	/**
	 * VerifyProductAppearsCart(String Search = null)
	 * @param Search - User should send a search for parts name 
	 * @return true if product appears in the cart, otherwise false
	 */

	@Keyword
	def VerifyProductAppearsCart(String Search = null) {

		KeywordUtil.logInfo("Verify product appears in the cart")

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Cart"),20)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Search/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Cart/Print Quote"),20)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject to = new TestObject("productAppearsCart")
		String xpath = "//div[@class='col-xs-12 col-sm-8 info']//p[@class='productName'][contains(text(),'" + Search + "')]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new tcom.utils()).verifyVisibleOfElement(to,30)) {
			KeywordUtil.logInfo("Can see the product appears in the cart : " + Search)
			return false
		}

		KeywordUtil.logInfo("End Search product under My tcom page")
		return true
	}


	/**
	 * importSKUFromFile
	 * @param 
	 * @param 
	 * @param ProjectName - is the name of the project to create... send null if using system created Date or specifying a prefix in global variables
	 * @return true if created, otherwise false
	 */

	@Keyword
	def importSKUFromFile(String partsName = null, String qualitySize = null) {

		String uploadFilePath = new File('').absolutePath + '\\Template\\Sample.csv'
		String Path = ('C:\\Users\\' + System.getProperty('user.name')) + '\\Downloads\\Sample.csv'
		def file = new File(Path)

		KeywordUtil.logInfo("Verify the Import from File page displays")

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Cart"),30)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Search/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Cart/Import"),30)) {
			KeywordUtil.logInfo("Cannot see the Import")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Import"))) {
			KeywordUtil.logInfo("Cannot click Import")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Cart/Import from File"),30)) {
			KeywordUtil.logInfo("Cannot see the Import from File")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Download Sample CSV File"))) {
			KeywordUtil.logInfo("Cannot click Download Sample CSV File")
			return false
		}

		if (!(new tcom.utils().delay(10))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(file.exists())) {
			KeywordUtil.logInfo("Cannot Download Sample CSV File")
			return false
		}

		if (!(file.delete())) {
			KeywordUtil.logInfo("Cannot Delete the Download Sample CSV File")
			return false
		}

		if (!(new tcom.utils().delay(3))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(new tcom.utils().writeToCSVFile(uploadFilePath, partsName, qualitySize))) {
			KeywordUtil.logInfo("Cannot create Project Template file using CSV File")
			return false
		}

		if (!(new tcom.utils().delay(3))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}
		if (!(new tcom.utils()).sendKeysOutWait(findTestObject("Cart/Import File"),uploadFilePath)) {
			KeywordUtil.logInfo("Cannot enter Import File path")
			return false
		}


		if (!(new tcom.utils()).click(findTestObject("Cart/Upload and Import"))) {
			KeywordUtil.logInfo("Cannot click Upload and Import")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject to = new TestObject("Project Name")
		String xpath = "//div[@class='col-xs-12 col-sm-8']//li[text()='" + partsName + "']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)


		if(!(new tcom.utils()).verifyVisibleOfElement(to,30)){
			KeywordUtil.logInfo("Cannot see the Upload SKUs name")
			return false
		}

		KeywordUtil.logInfo("Verified the Import from File page displays and CSV file is uploaded")
		return true
	}

	/**
	 * importSKUFromManually
	 * @param
	 * @param
	 * @param ProjectName - is the name of the project to create... send null if using system created Date or specifying a prefix in global variables
	 * @return true if created, otherwise false
	 */

	@Keyword
	def importSKUFromManually(String partsName = null, String qualitySize = null) {

		KeywordUtil.logInfo("Verify the Import from SKU in Manually page displays")

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Cart"),30)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Search/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Cart/Import"),30)) {
			KeywordUtil.logInfo("Cannot see the Import")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Import"))) {
			KeywordUtil.logInfo("Cannot click Import")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Cart/Import from File"),30)) {
			KeywordUtil.logInfo("Cannot see the Import from File")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Cart/SKU 1"),partsName)) {
			KeywordUtil.logInfo("Cannot enter SKU 1")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Cart/QTY 1"),qualitySize)) {
			KeywordUtil.logInfo("Cannot enter QTY 1")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Import Manual"))) {
			KeywordUtil.logInfo("Cannot click Import Manual")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}


		TestObject to = new TestObject("Project Name")
		String xpath = "//div[@class='col-xs-12 col-sm-8']//li[text()='" + partsName + "']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if(!(new tcom.utils()).verifyVisibleOfElement(to,30)){
			KeywordUtil.logInfo("Cannot see the Upload SKUs name")
			return false
		}

		KeywordUtil.logInfo("Verified the Import from Manually page displayed")
		return true
	}

	/**
	 * updatedQtyRemovedCart() 
	 * @return true if user is able to update quality no and validate the total price changes and shoul remove the parts from cart, otherwise false
	 */

	@Keyword
	def updatedQtyRemovedCart() {

		KeywordUtil.logInfo("Verify the quantity is changed and the price is changed and Item has been removed from the cart")

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Cart"),30)) {
			KeywordUtil.logInfo("Cannot see Cart")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Search/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Import"),30)) {
			KeywordUtil.logInfo("Cannot see the Curated Lists")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).delay(10)) {
			KeywordUtil.logInfo("Cannot wait a particular second")
			return false
		}


		String strProductXpath = "//div[@class='col-xs-12 col-sm-8 info']//p[@class='productName']"
		ArrayList<WebElement> arrList = (new tcom.utils()).listWebElement(strProductXpath)
		String strFirstProductName = arrList.get(0).getText().trim()

		TestObject toTotal = new TestObject("Product Quality Total")
		String strXpathFirstProductTotal = "//div[@class='col-xs-12 col-sm-8 info']//p[@class='productName'][contains(text(),'" + strFirstProductName + "')]/following::p[2]"
		TestObjectProperty propTotal = new TestObjectProperty("xpath", ConditionType.EQUALS, strXpathFirstProductTotal)
		toTotal.addProperty(propTotal)

		String strTotalPriceBefore = (new tcom.utils()).sReturnText(toTotal)
		println strTotalPriceBefore

		TestObject toProductQuality = new TestObject("Product Quality Update")
		String strXpathFirstProductName = "//div[@class='col-xs-12 col-sm-8 info']//p[@class='productName'][contains(text(),'" + strFirstProductName + "')]/following::input[1]"
		TestObjectProperty propProductQuality = new TestObjectProperty("xpath", ConditionType.EQUALS, strXpathFirstProductName)
		toProductQuality.addProperty(propProductQuality)

		String strProductQualityBefore = (new tcom.utils()).GetAttributeText(toProductQuality, "value")
		println strProductQualityBefore

		if (!(new tcom.utils()).clearsendKeys(toProductQuality, "11")) {
			KeywordUtil.logInfo("Cannot enter Product Quality")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Save and Update"))) {
			KeywordUtil.logInfo("Cannot click Save and Update")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Import"),30)) {
			KeywordUtil.logInfo("Cannot see the Curated Lists")
			return false
		}

		TestObject toProductQualityAfter = new TestObject("Product Quality Update")
		String strXpathProductQualityAfter = "//div[@class='col-xs-12 col-sm-8 info']//p[@class='productName'][contains(text(),'" + strFirstProductName + "')]/following::input[1]"
		TestObjectProperty propProductQualityAfter = new TestObjectProperty("xpath", ConditionType.EQUALS, strXpathProductQualityAfter)
		toProductQualityAfter.addProperty(propProductQualityAfter)


		String strProductQualityAfter = (new tcom.utils()).GetAttributeText(toProductQualityAfter, "value")

		println strProductQualityAfter

		if ((strProductQualityBefore.trim().equalsIgnoreCase(strProductQualityAfter.trim()))) {
			KeywordUtil.logInfo("Can not add the cart in Curated Lists and cart Count : " + strProductQualityAfter)
			//return false
		}

		String strTotalPriceAfter =  (new tcom.utils()).sReturnText(toTotal)
		println strTotalPriceAfter

		if ((strTotalPriceBefore.trim().equalsIgnoreCase(strTotalPriceAfter.trim()))) {
			KeywordUtil.logInfo("Can not add the cart in Curated Lists and cart Count : " + strTotalPriceBefore)
			//return false
		}

		TestObject toProductRemove = new TestObject("Product Remove")
		String strXpathProductRemove = "//div[@class='col-xs-12 col-sm-8 info']//p[@class='productName'][contains(text(),'" + strFirstProductName + "')]/preceding::label[1]"
		TestObjectProperty propProductRemove = new TestObjectProperty("xpath", ConditionType.EQUALS, strXpathProductRemove)
		toProductRemove.addProperty(propProductRemove)

		if (!(new tcom.utils()).click(toProductRemove)) {
			KeywordUtil.logInfo("Cannot click Remove product")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Cart/Remove Selected"))) {
			KeywordUtil.logInfo("Cannot click Remove Selected")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Import"),30)) {
			KeywordUtil.logInfo("Cannot see the Curated Lists")
			return false
		}

		/*	String strXpathProduct = "//div[@class='col-xs-12 col-sm-8 info']//p[@class='productName'][contains(text(),'" + strFirstProductName + "')]"
		 TestObjectProperty propProduct = new TestObjectProperty("xpath", ConditionType.EQUALS, strXpathProduct)
		 if (!(new tcom.utils()).verifyElementDidNotDisplayed(strXpathProduct)) {
		 KeywordUtil.logInfo("Can see the parts name : " + strFirstProductName)
		 return false
		 }
		 */
		KeywordUtil.logInfo("Verified the quantity is changed and the price is changed and Item has been removed from the cart")
		return true
	}

	/**
	 * ClearCart()
	 * @return true if Cart is Clear, otherwise false
	 */

	@Keyword
	def ClearCart() {

		KeywordUtil.logInfo("Verify the Clear the Cart")

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Cart"),30)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Search/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Cart/Import"),30)) {
			KeywordUtil.logInfo("Cannot see the Import")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		ArrayList<WebElement> lst = (new tcom.utils()).listWebElement("//div[@class='itemsHeader']//h5[contains(text(),'Backordered')]/preceding::div[@class='checkbox'][1]")
		if(lst.size() == 1){

			if (!(new tcom.utils()).click(findTestObject("Cart/Backordered"))) {
				KeywordUtil.logInfo("Cannot click Back ordered")
				return false
			}
			println("Hi")
			/*if (!(new tcom.utils()).click(findTestObject("Cart/RemoveSelected"))) {
			 KeywordUtil.logInfo("Cannot click Remove Selected")
			 return false
			 }*/
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}


		KeywordUtil.logInfo("Verified the Clear the Cart")
		return true
	}
}
