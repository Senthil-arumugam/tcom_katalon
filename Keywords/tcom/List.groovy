



package tcom
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable

class List {

	/**
	 * getListName
	 * @return username
	 */
	@Keyword
	def String getListName(String sListName = null) {

		String strListName = ""
		strListName = (new tcom.utils()).getName().trim()
		if(!(sListName == null)){
			strListName = GlobalVariable.GlobalUserName + sListName + "list"
		} else {
			strListName = GlobalVariable.GlobalUserName + "list"
		}

		KeywordUtil.logInfo("List Name : " + strListName)
		return strListName.toLowerCase()
	}


	/**
	 * CreateList(String strListName = null) 
	 * @param strListName - list name 
	 * @return true if list is created and validated the presents, otherwise false
	 */


	@Keyword
	def CreateList(String strListName = null) {

		String sListName = ""
		KeywordUtil.logInfo("Verify the list has been created")

		sListName = (new tcom.List()).getListName(strListName).trim()

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/AccountAndLocationText"),30)) {
			KeywordUtil.logInfo("Cannot see Account Button")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/AccountAndLocationText"))) {
			KeywordUtil.logInfo("Cannot click Choose Location Continue")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Lists/Your Lists"),30)) {
			KeywordUtil.logInfo("Cannot see the Your Lists")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/Your Lists"))) {
			KeywordUtil.logInfo("Cannot click Your Lists")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Lists/Create New"),30)) {
			KeywordUtil.logInfo("Cannot see the Create New")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/Create New"))) {
			KeywordUtil.logInfo("Cannot click Create New")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Lists/Create New List"),30)) {
			KeywordUtil.logInfo("Cannot see the Create New")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Lists/New List Name"), sListName)) {
			KeywordUtil.logInfo("Cannot enter New List Name")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/Create"))) {
			KeywordUtil.logInfo("Cannot click Create")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		KeywordUtil.logInfo("Verified the list is created")
		return true
	}

	/**
	 * CreateList(String strListName = null)
	 * @param strListName - User should send a List Name
	 * @return true if list is created, otherwise false
	 */

	@Keyword
	def VerifyList(String strListName = null) {

		String sListName = ""
		KeywordUtil.logInfo("Verify the created list presents")

		sListName = (new tcom.List()).getListName(strListName).trim()

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/AccountAndLocationText"),30)) {
			KeywordUtil.logInfo("Cannot see Account Button")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/AccountAndLocationText"))) {
			KeywordUtil.logInfo("Cannot click Choose Location Continue")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Lists/Your Lists"),30)) {
			KeywordUtil.logInfo("Cannot see the Your Lists")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/Your Lists"))) {
			KeywordUtil.logInfo("Cannot click Your Lists")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Lists/Create New"),30)) {
			KeywordUtil.logInfo("Cannot see the Create New")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject to = new TestObject("CreatedList")
		String xpath = "//a[text()='" + sListName + "']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new tcom.utils()).verifyVisibleOfElement(to,30)) {
			KeywordUtil.logInfo("Can see the created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click created list Name : " + sListName)
			return false
		}

		KeywordUtil.logInfo("Verified the created list is displayed and List Name " + sListName)
		return true
	}

	/**
	 * importProducts(String strListName = null, String partsName = null, String qualitySize = null)
	 * @param strListName - list name
	 * @param partsName - parts number
	 * @param qualitySize - quality size of particular parts 
	 * @return true if list is created and validated the presents, otherwise false
	 */

	@Keyword
	def importProducts(String strListName = null, String partsName = null, String qualitySize = null) {

		String uploadFilePath = new File('').absolutePath + '\\Template\\Sample.csv'
		String Path = ('C:\\Users\\' + System.getProperty('user.name')) + '\\Downloads\\Sample.csv'
		def file = new File(Path)

		KeywordUtil.logInfo("Verify the Import from File page displays")

		String sListName = (new tcom.List()).getListName(strListName).trim()

		if (!(new tcom.List()).VerifyList(strListName)) {
			KeywordUtil.logInfo("Can not navigate the created your list page")
			return false
		}

		TestObject to = new TestObject("CreatedList")
		String xpath ="//*[contains(text(),'" + sListName + "')]/following::div[@class='btn btn-outline corePurple moreButton hidden-xs'][1]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new tcom.utils()).verifyVisibleOfElement(to,30)) {
			KeywordUtil.logInfo("Cannot see created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Lists/Import"),30)) {
			KeywordUtil.logInfo("Cannot see Import")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/Import"))) {
			KeywordUtil.logInfo("Cannot click Import")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Lists/Import Products"),30)) {
			KeywordUtil.logInfo("Cannot see the Import Products")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/here"))) {
			KeywordUtil.logInfo("Cannot click here")
			return false
		}

		if (!(new tcom.utils().delay(10))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(file.exists())) {
			KeywordUtil.logInfo("Cannot Download Sample CSV File")
			return false
		}

		if (!(file.delete())) {
			KeywordUtil.logInfo("Cannot Delete the Download Sample CSV File")
			return false
		}

		if (!(new tcom.utils()).writeToCSVFile(uploadFilePath, partsName, qualitySize)) {
			KeywordUtil.logInfo("Cannot create Project Template file using CSV File")
			return false
		}

		if (!(new tcom.utils().delay(1))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(new tcom.utils()).sendKeysOutWait(findTestObject("Lists/Import File"),uploadFilePath)) {
			KeywordUtil.logInfo("Cannot enter Import File")
			return false
		}


		if (!(new tcom.utils()).click(findTestObject("Lists/Import Import"))) {
			KeywordUtil.logInfo("Cannot click Import")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject toProduct = new TestObject("SKU No")
		String xpathProduct = "//*[text()='" + partsName + "']"
		TestObjectProperty propProduct = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathProduct)
		toProduct.addProperty(propProduct)

		if(!(new tcom.utils()).verifyVisibleOfElement(toProduct,30)){
			KeywordUtil.logInfo("Cannot see the Upload SKUs name")
			return false
		}

		KeywordUtil.logInfo("Verified the Import from File page displays and CSV file is uploaded")
		return true
	}

	/**
	 * duplicateProducts(String strListName = null, String partsName = null, String qualitySize = null)
	 * @param strListName - list name
	 * @param partsName - parts number
	 * @param qualitySize - quality size of particular parts
	 * @return true if list is created and validated the presents, otherwise false
	 */

	@Keyword
	def duplicateProducts(String strListName = null, String partsName = null, String qualitySize = null) {


		KeywordUtil.logInfo("Verify the Import from File page displays")

		String sListName = (new tcom.List()).getListName(null).trim()
		String duplicateListName = (new tcom.List()).getListName(strListName).trim()

		if (!(new tcom.List()).VerifyList(null)) {
			KeywordUtil.logInfo("Can not navigate the created your list page")
			return false
		}

		TestObject to = new TestObject("CreatedList")
		String xpath ="//*[contains(text(),'" + sListName + "')]/following::div[@class='btn btn-outline corePurple moreButton hidden-xs'][1]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new tcom.utils()).verifyVisibleOfElement(to,30)) {
			KeywordUtil.logInfo("Cannot see created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Lists/Duplicate"),30)) {
			KeywordUtil.logInfo("Cannot see Duplicate")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/Duplicate"))) {
			KeywordUtil.logInfo("Cannot click Duplicate")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Lists/Duplicate List"),30)) {
			KeywordUtil.logInfo("Cannot see the Duplicate List")
			return false
		}

		if (!(new tcom.utils()).sendKeys(findTestObject("Lists/Duplicate List Name"),duplicateListName)) {
			KeywordUtil.logInfo("Cannot enter Duplicate List Name")
			return false
		}


		if (!(new tcom.utils()).click(findTestObject("Lists/Duplicate Duplicate"))) {
			KeywordUtil.logInfo("Cannot click Duplicate Duplicate")
			return false
		}


		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject toProduct = new TestObject("partsName")
		String xpathProduct = "//*[text()='" + partsName + "']"
		TestObjectProperty propProduct = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathProduct)
		toProduct.addProperty(propProduct)

		if(!(new tcom.utils()).verifyVisibleOfElement(toProduct,30)){
			KeywordUtil.logInfo("Cannot see the SKUs name : "+ partsName)
			return false
		}

		TestObject toCart = new TestObject("Add to Cart")
		String xpathCart = "//*[contains(text(),'"+ duplicateListName +"')]/following::*[contains(text(),'Add to Cart')]"
		TestObjectProperty propCart = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathCart)
		toCart.addProperty(propCart)


		if (!(new tcom.utils()).click(toCart)) {
			KeywordUtil.logInfo("Cannot click Add to Cart and List name")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		KeywordUtil.logInfo("Verified the Import from File page displays and CSV file is uploaded")
		return true
	}

	/**
	 * VerifyProductAppearsCart(String Search = null)
	 * @param Search - User should send a search for parts name
	 * @return true if product appears in the cart, otherwise false
	 */

	@Keyword
	def VerifyProductAppearsCart(String partsNo = null, String quality = null) {

		KeywordUtil.logInfo("Verify product appears in the cart")

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Search/Cart"),20)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Search/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Cart/Print Quote"),20)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject to = new TestObject("partsNo")
		String xpath = "//div[@class='col-xs-12 col-sm-8']//li[text()='"+ partsNo +"']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new tcom.utils()).verifyVisibleOfElement(to,30)) {
			KeywordUtil.logInfo("Can see the SKUs number : " + partsNo)
			return false
		}

		TestObject toQty = new TestObject("quality")
		String xpathQty = "//div[@class='col-xs-12 col-sm-8']//li[text()='"+ partsNo +"']/preceding::input[@class='form-control'][1]"
		TestObjectProperty propQty = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathQty)
		toQty.addProperty(propQty)

		if (!(new tcom.utils()).GetAttributeText(toQty,"value").contains(quality)) {
			KeywordUtil.logInfo("Cannot get the QTY : " + quality)
			return false
		}


		KeywordUtil.logInfo("Verify the items from the list added to the cart appear")
		return true
	}

	/**
	 * updateQuantity(String strListName = null, String partsName = null, String qualitySize = null)
	 * @param strListName - list name
	 * @param partsName - parts number
	 * @param qualitySize - quality size of particular parts
	 * @return true if list is created and validated the presents, otherwise false
	 */

	@Keyword
	def updateQuantity(String strListName = null, String partsName = null, String qualitySize = null) {


		KeywordUtil.logInfo("Verify the Import from File page displays")

		String sListName = (new tcom.List()).getListName(strListName).trim()

		if (!(new tcom.List()).VerifyList(strListName)) {
			KeywordUtil.logInfo("Can not navigate the created your list page")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject toList = new TestObject("CreatedList")
		String xpathList ="//a[text()='" +sListName+ "']"
		TestObjectProperty propList = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathList)
		toList.addProperty(propList)

		if (!(new tcom.utils()).verifyVisibleOfElement(toList,30)) {
			KeywordUtil.logInfo("Cannot see created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).click(toList)) {
			KeywordUtil.logInfo("Cannot click created list Name : " + sListName)
			return false
		}

		TestObject to = new TestObject("CreatedList")
		String xpath = "//*[contains(text(),'"+ sListName +"')]/following::input[@data-bind='textInput: runs() ? runs : quantity']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new tcom.utils()).verifyVisibleOfElement(to,30)) {
			KeywordUtil.logInfo("Cannot see created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).clearsendKeys(to, qualitySize)) {
			KeywordUtil.logInfo("Cannot enter the QTY : " + qualitySize)
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/Update Quantity"))) {
			KeywordUtil.logInfo("Cannot click Duplicate")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}


		TestObject toProduct = new TestObject("SKU No")
		String xpathProduct = "//li[text()='" + partsName + "']"
		TestObjectProperty propProduct = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathProduct)
		toProduct.addProperty(propProduct)


		if(!(new tcom.utils()).verifyVisibleOfElement(toProduct,30)){
			KeywordUtil.logInfo("Cannot see the Upload SKUs name")
			return false
		}

		if (!(new tcom.utils()).GetAttributeText(to,"value").contains(qualitySize)) {
			KeywordUtil.logInfo("Cannot get the QTY : " + qualitySize)
			return false
		}

		/*	TestObject toCart = new TestObject("Add to Cart")
		 String xpathCart = "//*[contains(text(),'"+ sListName +"')]/following::*[contains(text(),'Add to Cart')]"
		 TestObjectProperty propCart = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathCart)
		 toCart.addProperty(propCart)
		 if (!(new tcom.utils()).click(toCart)) {
		 KeywordUtil.logInfo("Cannot click Add to Cart and List name")
		 return false
		 }
		 if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
		 KeywordUtil.logInfo("Can see the Load image")
		 return false
		 }*/
		KeywordUtil.logInfo("Verify the quantity has been updated")
		return true
	}

	/**
	 * removedPartsFromList(String strListName = null, String partsName = null, String qualitySize = null)
	 * @param strListName - list name
	 * @param partsName - parts number
	 * @param qualitySize - quality size of particular parts
	 * @return true if  item is removed from the list, otherwise false
	 */

	@Keyword
	def removedList(String strListName = null, String partsName = null) {


		KeywordUtil.logInfo("Verify the Duplicate list is removed")

		String sListName = (new tcom.List()).getListName(strListName).trim()

		if (!(new tcom.List()).VerifyList(null)) {
			KeywordUtil.logInfo("Can not navigate the created your list page")
			return false
		}

		TestObject toList = new TestObject("Created List")
		String xpathList = "//a[text()='" + sListName + "']"
		TestObjectProperty propList = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathList)
		toList.addProperty(propList)


		if (!(new tcom.utils()).verifyVisibleOfElement(toList,30)) {
			KeywordUtil.logInfo("Can see the created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(toList)) {
			KeywordUtil.logInfo("Cannot click created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject to = new TestObject("CreatedList")
		String xpath ="//*[contains(text(),'" + sListName + "')]/following::div[@class='btn btn-outline corePurple moreButton hidden-xs'][1]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new tcom.utils()).verifyVisibleOfElement(to,30)) {
			KeywordUtil.logInfo("Cannot see created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Lists/Delete"),30)) {
			KeywordUtil.logInfo("Cannot see Delete")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/Delete"))) {
			KeywordUtil.logInfo("Cannot click Delete")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Lists/Continue"),30)) {
			KeywordUtil.logInfo("Cannot see the Continue")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Lists/Continue"))) {
			KeywordUtil.logInfo("Cannot click Continue")
			return false
		}


		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject toProduct = new TestObject("List Name")
		String xpathProduct = "//h4[text()='" + sListName + "']"
		TestObjectProperty propProduct = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathProduct)
		toProduct.addProperty(propProduct)

		if(!(new tcom.utils()).verifyElementNotPresent(toProduct,30)){
			KeywordUtil.logInfo("Cannot see the SKUs name : "+ partsName)
			return false
		}

		KeywordUtil.logInfo("Verified the Duplicate list is removed")
		return true
	}

	/**
	 * removedList(String strListName = null, String partsName = null, String qualitySize = null)
	 * @param strListName - list name
	 * @param partsName - parts number
	 * @param qualitySize - quality size of particular parts
	 * @return true if created list is removed from the list, otherwise false
	 */

	@Keyword
	def removedPartsFromList(String strListName = null, String partsName = null) {


		KeywordUtil.logInfo("Verify item is removed from the list")

		String sListName = (new tcom.List()).getListName(strListName).trim()

		if (!(new tcom.List()).VerifyList(strListName)) {
			KeywordUtil.logInfo("Can not navigate the created your list page")
			return false
		}

		TestObject to = new TestObject("CreatedList")
		String xpath ="//*[contains(text(),'" + sListName + "')]/following::div[@class='btn btn-outline corePurple moreButton hidden-xs'][1]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new tcom.utils()).verifyVisibleOfElement(to,30)) {
			KeywordUtil.logInfo("Cannot see created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject toProduct = new TestObject("partsName")
		String xpathProduct = "//*[contains(text(),'" + sListName + "')]/following::a[@class='remove'][1]"
		TestObjectProperty propProduct = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathProduct)
		toProduct.addProperty(propProduct)


		if (!(new tcom.utils()).verifyVisibleOfElement(toProduct,30)) {
			KeywordUtil.logInfo("Cannot see created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).click(toProduct)) {
			KeywordUtil.logInfo("Cannot click created list Name : " + sListName)
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		TestObject toParts = new TestObject("partsName")
		String xpathParts = "//*[text()='" + partsName + "']"
		TestObjectProperty propParts = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathParts)
		toParts.addProperty(propParts)

		if(!(new tcom.utils()).verifyElementNotPresent(toParts,30)){
			KeywordUtil.logInfo("Cannot see the SKUs No : "+ partsName)
			return false
		}

		KeywordUtil.logInfo("Verified item is removed from the list")
		return true
	}


	/**
	 * CreateList(String strListName = null)
	 * @param strListName - list name
	 * @return true if list is created and validated the presents, otherwise false
	 */


	@Keyword
	def curatedLists(String strListName = null, String sCuratedListsChoice = null) {

		String sListName = ""
		KeywordUtil.logInfo("Verify a list of products can be viewed within the chosen curated list")

		sListName = (new tcom.List()).getListName(strListName).trim()

		if (!(new tcom.utils()).verifyVisibleOfElement(findTestObject("Login/AccountAndLocationText"),30)) {
			KeywordUtil.logInfo("Cannot see Account Button")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Login/AccountAndLocationText"))) {
			KeywordUtil.logInfo("Cannot click Choose Location Continue")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Curated Lists/Curated Lists"),30)) {
			KeywordUtil.logInfo("Cannot see the Curated Lists")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Curated Lists/Curated Lists"))) {
			KeywordUtil.logInfo("Cannot click Curated Lists")
			return false
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Curated Lists/Badge Test"),30)) {
			KeywordUtil.logInfo("Cannot see the Badge Test")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		String strCartBefore = (new tcom.utils()).sReturnText(findTestObject("Curated Lists/Cart Count"))

		if(sCuratedListsChoice.equalsIgnoreCase("Badge Test")){

			if (!(new tcom.utils()).click(findTestObject("Curated Lists/Badge Test"))) {
				KeywordUtil.logInfo("Cannot click Badge Test")
				return false
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}

			if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Curated Lists/Badge Test Heading"),30)) {
				KeywordUtil.logInfo("Cannot see the Badge Test Heading")
				return false
			}
		}
		else if(sCuratedListsChoice.equalsIgnoreCase("Cables")){

			if (!(new tcom.utils()).click(findTestObject("Curated Lists/Cables"))) {
				KeywordUtil.logInfo("Cannot click Cables")
				return false
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}

			if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Curated Lists/Cables Heading"),30)) {
				KeywordUtil.logInfo("Cannot see the Cables Heading")
				return false
			}
		}
		else if(sCuratedListsChoice.equalsIgnoreCase("Cut Cable")){

			if (!(new tcom.utils()).click(findTestObject("Curated Lists/Cut Cable"))) {
				KeywordUtil.logInfo("Cannot click Cut Cable")
				return false
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}

			if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Curated Lists/Cut Cable Heading"),30)) {
				KeywordUtil.logInfo("Cannot see the Cut Cable Heading")
				return false
			}
		}
		else if(sCuratedListsChoice.equalsIgnoreCase("New Public List")){

			if (!(new tcom.utils()).click(findTestObject("Curated Lists/New Public List"))) {
				KeywordUtil.logInfo("Cannot click New Public List")
				return false
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}

			if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Curated Lists/New Public List Heading"),30)) {
				KeywordUtil.logInfo("Cannot see the New Public List Heading")
				return false
			}
		}
		else if(sCuratedListsChoice.equalsIgnoreCase("Samsung CES")){

			if (!(new tcom.utils()).click(findTestObject("Curated Lists/Samsung CES"))) {
				KeywordUtil.logInfo("Cannot click Samsung CES 2018 Innovation Award winners")
				return false
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}

			if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Curated Lists/Samsung CES Heading"),30)) {
				KeywordUtil.logInfo("Cannot see the Samsung CES Heading")
				return false
			}
		} else {
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new tcom.utils()).click(findTestObject("Curated Lists/Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click Add to Cart")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if ((new tcom.utils()).verifyElementIsDisplayed(findTestObject("Cart/Number of Cuts"),10)) {
			if (!(new tcom.search()).VerifyCableCut()) {
				KeywordUtil.logInfo("Cannot Add Reel Lengths in CableCut")
				return false
			}

			if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
				KeywordUtil.logInfo("Can see the Load image")
				return false
			}
		}

		if (!(new tcom.utils()).verifyElementIsDisplayed(findTestObject("Curated Lists/Cart Count"),30)) {
			KeywordUtil.logInfo("Cannot see the Close")
			return false
		}

		if (!(new tcom.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		String strCartAfter = (new tcom.utils()).sReturnText(findTestObject("Curated Lists/Cart Count"))
		if ((strCartBefore.equalsIgnoreCase(strCartAfter))) {
			KeywordUtil.logInfo("Can not add the cart in Curated Lists and cart Count : " + strCartAfter)
			return false
		}

		KeywordUtil.logInfo("Verified the list of products can be viewed within the chosen curated list")
		return true
	}
}
