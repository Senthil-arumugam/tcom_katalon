<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TCOM Test Suite stage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1106e1ef-77bf-4a46-a42f-9f7f2301202a</testSuiteGuid>
   <testCaseLink>
      <guid>adb2fbb0-6bde-4890-90d9-dda8d52b36e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Registration/TC2.1_Validate the Registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dd9140a-f44e-4678-ae0a-4dcdf9a84e36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Registration/TC2.2_Validate the Registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7aedb102-2824-42b4-8315-8267294f9381</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/TC6_Search For Flex Product and validate Parts and Add to Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>406d9b01-15b7-4b28-b706-ba8802973b5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC7_Verify Import SKU from File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9159fce-a163-4f8f-a38c-29cf40257b1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC8_Verify Import SKU from Manually</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>264bb39f-a352-488d-b1fd-236d7a34a474</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/TC9_Search For Cut cable Product and Add to Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d37ec8af-aa28-46db-9ad7-16e9350dbd66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/TC10_Search For Tunable Antenna Product and Add to Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1bcc8a2-67a1-427e-8da6-d72b8f1ab0bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/TC11_Search For Cable Jumper Builder Product and Add to Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26d4394d-032f-41c0-97df-9cec09d37d39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lists/TC13_Created a List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d0573d3-ebad-4360-9533-7c431d26534c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lists/TC14_Verify the list has been created</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b6beab4-9aeb-4bd6-897d-b579a97f1622</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lists/TC15_Create and Verify the Import Products items populated the list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a37b86f2-c6c7-4f86-8f4e-d6cf26c21b72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lists/TC16_Create Duplicate list and Verify the Duplicate products populated list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b8edfaf-7973-4b8f-9cfc-fe71056815e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lists/TC17_Verify Product Appears In The Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcc8628b-c470-40cc-baa8-cc0a89f03c2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lists/TC18_Change the quantity of any item and Update Quantity</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f49f9828-25b6-4c7b-89c3-90163062f53e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lists/TC21_Verify the Duplicate list and removed the list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31653989-a1f9-4091-a01a-b6f2b92d963c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lists/TC19_Verify the list and item is removed from the list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e8c2cbb-ce90-432d-8c3a-dd86ab135f6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lists/TC20_Verify the list and removed the list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d694fb95-9f1b-4406-914b-607c23790c99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Curated Lists/TC22_Verify the Curated Lists and Click Add to Cart Cables</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa2aa01a-5f6f-40c5-90d5-bc61a67e422f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Curated Lists/TC23_Verify the Curated Lists and Click Add to Cart Badge Test</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc1b3e54-c0d1-4bde-ae27-2832a025506b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Curated Lists/TC24_Verify the Curated Lists and Click Add to Cart Cut Cable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6546a599-4728-4840-bdf3-fb323db79f23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Curated Lists/TC25_Verify the Curated Lists and Click Add to Cart New Public List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccfa2510-fedb-44f9-982b-017a943edd3f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Curated Lists/TC26_Verify the Curated Lists and Click Add to Cart Samsung CES</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68a3ea1f-597c-4940-875b-1d52430b5b54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC27_Verify Qty updated and Item is remove from the cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d9d9720-d6e6-4532-b89d-5ab32b599df1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC28_Verify order can be placed successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccd925d2-2690-4380-98c1-fed6eacc4a96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/TC4_Search For Cable Product and validate Parts and Add to Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2b92bb0-9ce1-4f37-aa5f-458b076239e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search/TC5_Verify Product Appears In The Cart</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
